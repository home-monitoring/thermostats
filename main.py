import dht
import machine
import time
import urequests

d = dht.DHT11(machine.Pin(2))

# set RTC.ALARM0 to fire after 10 seconds (waking the device)
while True:
    print("Measuring")
    retry = 0
    while retry < 3:
        try:
            d.measure()
            break
        except:
            retry = retry + 1
            print(".", end = "")
    if retry < 3:
        print("Temperature: %3.1f °C" % d.temperature())
        print("Humidity: %3.1f" % d.humidity())
        temperature = urequests.post('https://io.adafruit.com/api/v2/scttrgd/feeds/home-data.temp-01/data?X-AIO-Key=d831bfda01d8428e97f427bbc0010c2d', json=dict(value=d.temperature()))
        humidity = urequests.post('https://io.adafruit.com/api/v2/scttrgd/feeds/home-data.humidity-01/data?X-AIO-Key=d831bfda01d8428e97f427bbc0010c2d', json=dict(value=d.humidity()))
        print(temperature.text)
        print(humidity.text)
        gc.enable()
        gc.collect()
    print('Sleeping for 10 minutes now')
    # time.sleep(2)
    time.sleep(600)
